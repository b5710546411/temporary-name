var Leviathan = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/null.png' );
        this.attackDirection = 'up';
        this.animeRound = 0;
        this.effect = new Effect();
        this.addChild( this.effect );
    },

    setStatus: function( Player, Room ) {
        this.player = Player;
        this.room = Room;
    },

    update: function() {
        var pPos = this.player.getPosition();
        console.log(pPos.x - ( this.room.getPosition().x - screenWidth ), pPos.x, this.room.getPosition().x);
        this.animeRound++;
        if ( Math.abs( this.player.speed ) == 5 ) {
            this.player.isDeath = true;
            this.attackDirection = 'none';
        }
        if ( !this.player.isDeath ) {
            if ( this.animeRound <= 1 ) {
                this.randomAttackDirection();
            }

            if ( this.attackDirection == 'up' ) {
                if ( this.animeRound <= 2 ) { this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , screenHeight - 49.5 ) ); }
                else if ( this.animeRound <= 62 ) {
                    if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 10 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/01U.png' ); this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , screenHeight - 49.5 ) ); }
                    else if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 20 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/02U.png' ); this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , screenHeight - 49.5 ) ); }
                    else if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 30 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/03U.png' ); this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , screenHeight - 49.5 ) ); }
                    if ( this.animeRound == 62 ) { this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , screenHeight - 49.5 + 360 ) );};
                } else if ( this.animeRound <= 74 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/1U.png' ); this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , this.getPosition().y - 30 ) );
                } else if ( this.animeRound <= 80 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/2U.png' );
                    if ( !this.player.isCrouch ) { this.animeRound = 0; this.player.isDeath = true; }
                } else if ( this.animeRound <= 92 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/1U.png' ); this.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth - 200 ) , this.getPosition().y + 30 ) );
                } else { this.animeRound = 0; this.initWithFile( 'res/Images/null.png' ); }
            } else if ( this.attackDirection == 'down' ) {
                if ( this.animeRound <= 2 ) { this.setPosition( new cc.Point( pPos.x + this.player.speed*80 - ( this.room.getPosition().x - screenWidth - 200 ), 250 ) );
                } else if ( this.animeRound <= 62 ) {
                    if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 10 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/01D.png' ); }
                    else if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 20 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/02D.png' ); }
                    else if ( ( ( this.animeRound - 2 ) % 30 ) + 1 <= 30 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/03D.png' ); }
                } else if ( this.animeRound <= 68 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/1D.png' ); }
                else if ( this.animeRound <= 74 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/2D.png' ); }
                else if ( this.animeRound <= 80 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/3D.png' ); 
                    if ( this.closeTo() ) { this.animeRound = 0; this.player.isDeath = true; this.effect.setPosition( new cc.Point( pPos.x - ( this.room.getPosition().x - screenWidth ) - this.getPosition().x + 220, pPos.y - 142 ) ); }
                } else if ( this.animeRound <= 86 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/2D.png' ); }
                else if ( this.animeRound <= 92 ) { this.initWithFile( 'res/Images/Enemy/Leviathan/1D.png' ); }
                else { this.animeRound = 0; this.initWithFile( 'res/Images/null.png' ); }
            }
        } else {
            this.playDeathAnimetion();
        }
    },

    playDeathAnimetion: function() {
        var pPos = this.player.getPosition();
        if ( this.attackDirection == 'up' ) {
            if ( this.animeRound <= 6 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/1U.png' ); }
            else if ( this.animeRound <= 12 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/2U.png' ); }
            else if ( this.animeRound <= 18 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/3U.png' ); }
            else if ( this.animeRound <= 24 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/4U.png' ); }
            else if ( this.animeRound <= 100 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/5U.png' ); }
            else if ( this.animeRound <= 101 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/6U.png' ); }
            else if ( this.animeRound <= 300 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/5U.png' ); }
            else if ( this.animeRound <= 301 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/6U.png' ); }
            else if ( this.animeRound <= 310 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/5U.png' ); }
            else if ( this.animeRound <= 311 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/6U.png' ); }
            else { this.animeRound = 24; }
        } else if ( this.attackDirection == 'down' ) {
            if ( this.animeRound <= 6 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/1D.png' ); this.effect.initWithFile( 'res/Images/Player/Leviathan/Blood1.png' ); }
            else if ( this.animeRound <= 8 ) { this.effect.initWithFile( 'res/Images/Player/Leviathan/Blood2.png' ); }
            else if ( this.animeRound <= 10 ) { this.effect.initWithFile( 'res/Images/Player/Leviathan/Blood3.png' ); }
            else if ( this.animeRound <= 12 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/2D.png' ); this.effect.initWithFile( 'res/Images/Player/Leviathan/Blood4.png' ); }
            else if ( this.animeRound <= 18 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/3D.png' ); }
        } else if ( this.attackDirection == 'none' ) {
            if ( this.animeRound <= 10 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/1.png' ); }
            else if ( this.animeRound <= 20 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/2.png' ); }
            else if ( this.animeRound <= 30 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/3.png' ); }
            else if ( this.animeRound <= 40 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/4.png' ); }
            else if ( this.animeRound <= 150 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/5.png' ); }
            else if ( this.animeRound <= 160 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/6.png' ); }
            else if ( this.animeRound <= 170 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/7.png' ); }
            else if ( this.animeRound <= 180 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/8.png' ); }
            else if ( this.animeRound <= 190 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/9.png' ); }
            else if ( this.animeRound <= 349 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/10.png' ); }
            else if ( this.animeRound <= 350 ) { this.player.initWithFile( 'res/Images/Player/Leviathan/11.png' ); }
            else { this.animeRound = 190 }
        }
    },

    randomAttackDirection: function() {
        var temp = Math.floor( Math.random() * 2 );
        if ( temp == 0 ) {
            this.attackDirection = 'down';
        } else {
            this.attackDirection = 'up';
        }
        if ( !this.player.isCrouch ) {
            this.attackDirection = 'up'
        }
    },

    closeTo: function() {
		var myPos = this.getPosition();
		var oPos = this.player.getPosition();
	  	return ( Math.abs( myPos.x - ( oPos.x - ( this.room.getPosition().x - screenWidth - 200 ) ) ) <= 40 );
    },
});