var Item = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/Items/Select.png' );
        this.fileSource = 'res/Images/Items/Select.png';
        this.type = 'type';
        this.name = 'name';
    },

    setStatus: function( type, name, path ) {
        this.type = type;
        this.name = name;
        this.fileSource = path;
        this.initWithFile( this.fileSource );
    },

    use: function( Player, Room ) {
		if ( this.type == 'recovery' ) {
			Player.HP += 30;
		} else if ( this.type == 'key' ) {
			if ( Room.door.closeTo( Player ) ) {
				if ( this.name == 'Old Key' ) {
					if ( Room.roomID == 2 ) {
						Room.door.isLock = false;
						var index = Player.itemList.indexOf( this );
						if (index > -1) {
							Player.usedItemList.push( this );
 							Player.itemList.splice(index, 1);
 							return true;
						}
					}
				}
			}
		} else if ( this.type == 'special' ) {
			if ( Room.trap.closeTo( Player ) ) {
				if ( this.name == 'Crystal' ) {
					if ( Room.roomID == 6 ) {
						Room.initWithFile( 'res/Images/Room/room1-4b.bmp' );
        				Room.roomInit( 2, 'room', 6, Player );
        				Room.door.isLock = false;
						var index = Player.itemList.indexOf( this );
						if ( index > -1 ) {
							Player.usedItemList.push( this );
 							Player.itemList.splice(index, 1);
 							return true;
						}
					}
				}
			}
		} else {
			return false;
		}
    },

    closeTo: function( Player ) {
		var myPos = this.getPosition();
		var oPos = Player.getPosition();
	  	return ( Math.abs( myPos.x - oPos.x ) <= 150 );
    },
});