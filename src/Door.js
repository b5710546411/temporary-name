var Door = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/null.png' );
        this.isLock = false;
        this.hitbox = this.getBoundingBoxToWorld();
    },

    setRoom: function( Room ) {
        this.room = Room;
    },

    closeTo: function( Player ) {
		var myPos = this.getPosition();
		var oPos = Player.getPosition();
        if ( this.room.type == 'pass' ) {
            return ( Math.abs( myPos.x - ( oPos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) ) ) <= 100 );
        } else {
            return ( Math.abs( myPos.x - oPos.x ) <= 84 );
        }
        //return ( oPos.x > this.hitbox.x && oPos.x < this.hitbox.x + this.hitbox.width )
    },
});