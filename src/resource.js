var res = {
	trap1: 'res/Images/Player/Trap1/1.png',
	trap2: 'res/Images/Player/Trap1/2.png',
	trap3: 'res/Images/Player/Trap1/3.png',
	trap4: 'res/Images/Player/Trap1/4.png',
	trap5: 'res/Images/Player/Trap1/5.png',
	trap6: 'res/Images/Player/Trap1/6.png',
	trap7: 'res/Images/Player/Trap1/7.png',
	leviathan1: 'res/Images/Player/Leviathan/1.png',
	leviathan2: 'res/Images/Player/Leviathan/2.png',
	leviathan3: 'res/Images/Player/Leviathan/3.png',
	leviathan4: 'res/Images/Player/Leviathan/4.png',
	leviathan5: 'res/Images/Player/Leviathan/5.png',
	leviathan6: 'res/Images/Player/Leviathan/6.png',
	leviathan7: 'res/Images/Player/Leviathan/7.png',
	leviathan8: 'res/Images/Player/Leviathan/8.png',
	leviathan9: 'res/Images/Player/Leviathan/9.png',
	leviathan10: 'res/Images/Player/Leviathan/10.png',
	leviathan11: 'res/Images/Player/Leviathan/11.png',
	leviathanU1: 'res/Images/Player/Leviathan/1U.png',
	leviathanU2: 'res/Images/Player/Leviathan/2U.png',
	leviathanU3: 'res/Images/Player/Leviathan/3U.png',
	leviathanU4: 'res/Images/Player/Leviathan/4U.png',
	leviathanU5: 'res/Images/Player/Leviathan/5U.png',
	leviathanU6: 'res/Images/Player/Leviathan/6U.png',
	leviathanD1: 'res/Images/Player/Leviathan/1D.png',
	leviathanD2: 'res/Images/Player/Leviathan/2D.png',
	leviathanD3: 'res/Images/Player/Leviathan/3D.png',
	blood1: 'res/Images/Player/Leviathan/Blood1.png',
	blood2: 'res/Images/Player/Leviathan/Blood2.png',
	blood3: 'res/Images/Player/Leviathan/Blood3.png',
	filter1: 'res/Images/Filter(Red).bmp',
	filter2: 'res/Images/Filter(Black).bmp'
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}