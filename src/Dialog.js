var Dialog = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/ConversationBox.png' );
        this.conversationNumber = 'none';
        this.order = 1;
        this.isTalk = false;
    },

    setStatus: function( Conversation, Room, Player, Face, Effect, Text, Text2 ) {
        this.conversation = Conversation;
        this.room = Room;
        this.player = Player;
        this.face = Face;
        this.effect = Effect;
        this.reserveTextField = Text;
        this.reserveTextField2 = Text2;
    },

    speak: function( conversationNumber ) {
        this.conversationNumber = conversationNumber;
        this.isTalk = true;
        this.talk();
    },

    talk: function() {
        if ( this.order == 0 ) {
            this.isTalk = false;
            this.setVisible( false );
            this.conversation.setString( '' );
            this.conversation.setFontSize( 32 );
            cc.director.resume();
            this.order++;
        } else if ( this.conversationNumber == 'StartDialog' ){
            if ( this.order == 1 ) {
                cc.director.pause();
                this.setVisible( true );
    		    this.conversation.setString( 'How much time has passed?' );
                this.order++;
            } else if ( this.order == 2 ) {
                this.conversation.setString( 'My eyes just got used to the darkness, so I can finally see my surroundings.' );
                this.order++;
            } else if ( this.order == 3 ) {
                this.conversation.setString( 'I’m in a stone room.' );
                this.order++;
            } else if ( this.order == 4 ) {
                this.conversation.setString( 'Small, cramped and dark. It is gloomy and unpleasant.' );
                this.order++;
            } else if ( this.order == 5 ) {
                this.conversation.setString( 'I should be in my own room. In my own bed, sleeping under my warm and cosy blankets.' );
                this.order++;
            } else if ( this.order == 6 ) {
                this.conversation.setString( 'Why am I in this creepy place?' );
                this.order++;
            } else if ( this.order == 7 ) {
                this.conversation.setString( '…if I don’t leave quickly…' );
                this.order++;
            } else if ( this.order == 8 ) {
                this.conversation.setString( 'I need to go swiftly. There is a voice telling me…' );
                this.order++;
            } else if ( this.order == 9 ) {
                this.conversation.setString( 'If I don’t act, nothing will change.' );
                this.order++;
            } else if ( this.order == 10 ) {
                this.effect.initWithFile( 'res/Images/null.png' );
                this.order = 0;
                this.conversationNumber = '';
                this.talk();
            }
        } else if ( this.conversationNumber == 'Trap3' ){
            if ( this.order == 1 ) {
                cc.director.pause();
                this.setVisible( true );
                this.conversation.setString( 'This... What\'s that weird sound?' );
                this.face.icon.initWithFile( 'res/Images/Face/Talk.png' );
                this.order++;
            } else if ( this.order == 2 ) {
                this.isTalk = false;
                this.setVisible( false );
                this.conversation.setString( '' );
                cc.director.resume();
                this.order++;
            } else if ( this.order == 3 ) {
                cc.director.pause();
                this.setVisible( true );
                this.isTalk = true;
                this.conversation.setString( '!?' );
                this.face.icon.initWithFile( 'res/Images/Face/Surprise.png' );
                this.order++;
            } else if ( this.order == 4 ) {
                this.conversation.setString( '!? \nThe ceiling... it\'s closing in?!' );
                this.order++;
            } else if ( this.order == 5 ) {
                this.conversation.setString( 'N...No!!' );
                this.face.icon.initWithFile( 'res/Images/Face/cry.png' );
                this.order++;
            } else if ( this.order == 6 ) {
                this.isTalk = false;
                this.setVisible( false );
                this.conversation.setString( '' );
                cc.director.resume();
                this.order++;
            } else if ( this.order == 7 ) {
                cc.director.pause();
                this.setVisible( true );
                this.isTalk = true;
                this.conversation.setString( 'H...\nHiii...' );
                this.face.icon.initWithFile( 'res/Images/Face/HeavyCry.png' );
                this.order++;
            } else if ( this.order == 8 ) {
                this.conversation.setString( 'I... It hurts!\nI don\'t want to die!!\nI don\'t want to die...' );
                this.order++;
            } else if ( this.order == 9 ) {
                this.conversation.setString( 'This is.... This is...\nI\'m going to die?\nAm... I going to die!?' );
                this.order++;
            } else if ( this.order == 10 ) {
                this.conversation.setString( '...Aaaa... HIAAAAA\nNOOOOOOOOOOOOOOOOOOOO\nOOOOOOOOOOOOOOO!' );
                this.face.icon.initWithFile( 'res/Images/Face/Hurt.png' );
                this.order++;
            } else if ( this.order == 11 ) {
                this.conversation.setFontSize( 40 );
                this.conversation.setString( 'HELP ME! DAD! MOMMMM!!!' );
                this.order++;
            } else if ( this.order == 12 ) {
                this.isTalk = false;
                this.setVisible( false );
                this.conversation.setString( '' );
                cc.director.resume();
                this.order++;
            } else if ( this.order == 13 ) {
                cc.director.pause();
                this.setVisible( true );
                this.isTalk = true;
                this.conversation.setFontSize( 72 );
                this.conversation.setString( 'GUH...   ghe...' );
                this.reserveTextField2.setString( 'ghk...' );
                this.reserveTextField2.setPosition( new cc.Point( 650, 550 ) );
                this.
                this.order = 0;
                this.conversationNumber = '';
            }
        } else if ( this.conversationNumber == 'RedCrawler' ){
            if ( this.order == 1 ) {
                cc.director.pause();
                this.setVisible( true );
                this.conversation.setString( 'No...  noooooooooo!!!' );
                this.face.icon.initWithFile( 'res/Images/Face/HeavyCry.png' );
                this.order++;
            } else if ( this.order == 2 ) {
                this.effect.initWithFile( 'res/Images/Filter(Red).bmp' );
                this.conversation.setString( 'Uggh...    Guh....' );
                this.face.icon.initWithFile( 'res/Images/Face/Injured.png' );
                this.order++;
            } else if ( this.order == 3 ) {
                this.conversation.setString( 'It hurts.... It\'s so hurts...' );
                this.order++;
            } else if ( this.order == 4 ) {
                this.conversation.setString( 'Please stop.... I beg you... please...' );
                this.order++;
            } else if ( this.order == 5 ) {
                this.conversation.setFontSize( 55 );
                this.conversation.setString( 'IYAAAAA.... NONONONONO.....' );
                this.face.icon.initWithFile( 'res/Images/Face/MoreHurt.png' );
                this.order++;
            } else if ( this.order == 6 ) {
                this.conversation.setString( 'GYAAAAAAAAAAAA!!!' );
                this.face.icon.initWithFile( 'res/Images/Face/MostHurt.png' );
                this.order = 0;
            }
        } else if ( this.conversationNumber == 'Stage1 End' ){
            if ( this.order == 1 ) {
                cc.director.pause();
                this.reserveTextField.setString( 'Stage One Cleared!!!' );
                this.reserveTextField.setPosition( new cc.Point( 400, 500 ) );
                this.reserveTextField2.setString( 'Death                    ' + this.player.deathCount + '\n\n\n\n\nTime Used                  ' + Math.floor( this.player.timeUsed/60 ) );
                this.reserveTextField2.setPosition( new cc.Point( 400, 300 ) );
                this.face.icon.setVisible( false );
                this.face.setVisible( false );
                this.order++;
            } else if ( this.order == 2 ) {
                this.conversation.setString( 'To Be Continued...' );
                this.reserveTextField.setString( '' );
                this.reserveTextField2.setString( '' );
            }
        }
    },
});