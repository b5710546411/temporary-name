var Face = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/Face/Border.png' );
        this.setColor( new cc.Color( 255, 255, 255, 255 ) );
    },

    setStatus: function( Player, Images ) {
        this.player = Player;
        this.icon = Images;
        this.icon.initWithFile( 'res/Images/Face/Normal.png' );
    },

    update: function() {
    	var lostHp = 100.00 - this.player.HP;

        this.setColor( new cc.Color( 255 - ( lostHp * 0.93 ), 255 - ( lostHp * 2.55 ), 255 - ( lostHp * 2.55 ), 255 ) );
        if ( lostHp >= 60 ) {
        	this.icon.initWithFile( 'res/Images/Face/LowHP.png' );
        } else {
            this.icon.initWithFile( 'res/Images/Face/Normal.png' );
        }
    },
});