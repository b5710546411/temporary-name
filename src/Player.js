var Player = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/Player/Movement/0L.png' );
        this.animeRound = 0;
        this.timeUsed = 0;
		this.itemList = [];
		this.usedItemList = [];
		this.isUsingItem = false;
		this.HP = 100.00;
		this.isDeath = false;
		this.deathCount = 0;
		this.speed = 2.5;
        this.isMove = false;
        this.isLeft = false;
        this.isRight = false;
        this.isRun = false;
        this.stamina = 240;
        this.isCrouch = false;
        this.isChange = 0;
        /**
        	this variable define which way the player is trying to change the room
        */
        this.lastDirectionFacing = 'left';
    },

	update: function( dt ) {
		this.timeUsed++;
		if ( this.isDeath ) {
			this.isMove = false;
	        this.isLeft = false;
    	    this.isRight = false;
        	this.isUsingItem = false;
		} else {
			if ( !( this.isLeft || this.isRight ) ) {
	    		this.isMove = false;
	    		this.speed = 0;
	    	} else if ( this.isLeft || this.isRight ) {
	    		this.isMove = true
	    	}

	    	if ( this.stamina <= 0 ) {
	    		this.isRun = false;
	    	}

	    	if ( this.isMove ) {
	    		this.animeRound++;
		    	if ( this.isRun ) {
		    		this.speed = 5;
			    	this.move();
			    	this.stamina--;
			    	if ( this.animeRound <= 7 ) { this.initWithFile( 'res/Images/Player/Movement/Run/1L.png' ); }
	                else if ( this.animeRound <= 14 ) { this.initWithFile( 'res/Images/Player/Movement/Run/2L.png' ); }
	                else if ( this.animeRound <= 21 ) { this.initWithFile( 'res/Images/Player/Movement/Run/3L.png' ); }
	                else if ( this.animeRound <= 28 ) { this.initWithFile( 'res/Images/Player/Movement/Run/4L.png' ); }
	                else if ( this.animeRound <= 35 ) { this.initWithFile( 'res/Images/Player/Movement/Run/5L.png' ); }
	                else if ( this.animeRound <= 42 ) { this.initWithFile( 'res/Images/Player/Movement/Run/6L.png' ); }
	                else { this.animeRound = 0; }
		    	} else if ( this.isCrouch ) {
		    		this.speed = 1.25;
			    	this.move();
			    	if ( this.animeRound <= 20 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/4L.png' ); }
		    	    else if ( this.animeRound <= 40 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/3L.png' ); }
		        	else if ( this.animeRound <= 60 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/5L.png' ); }
		        	else if ( this.animeRound <= 80 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/3L.png' ); }
	                else { this.animeRound = 0; }
		    	} else {
		    		this.speed = 2.5
			    	this.move();
			    	if ( this.animeRound <= 15 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/1L.png' ); }
	                else if ( this.animeRound <= 30 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/2L.png' ); }
	                else if ( this.animeRound <= 45 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/3L.png' ); }
	                else if ( this.animeRound <= 60 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/4L.png' ); }
	                else if ( this.animeRound <= 75 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/5L.png' ); }
	                else if ( this.animeRound <= 90 ) { this.initWithFile( 'res/Images/Player/Movement/Walk/6L.png' ); }
	                else { this.animeRound = 0; }
		    	}
	    	} else {
	    		this.stamina++;
	    		if ( this.isCrouch ) {
	    			this.initWithFile( 'res/Images/Player/Movement/Crouch/2L.png' );
	    			this.flip();
	    		}
	    		else if ( !this.isCrouching ) {
	    			this.initWithFile( 'res/Images/Player/Movement/0L.png' );
	    			this.flip();
	    		}
	    	}
			this.flip();
		}
    },

    move: function() {
    	var pos = this.getPosition();

    	if ( this.isLeft ) {
		    if ( this.speed > 0 ) {
		    	this.speed *= -1;
		    }
		   	if ( pos.x >= 100 ) {
			    this.setPosition( new cc.Point( pos.x + this.speed, pos.y ) );
			} else {
			    this.isChange = 1;
			}
		} else {
			if ( this.speed < 0 ) {
		    	this.speed *= -1;
		    }
			if ( pos.x <= screenWidth - 100 ) {
				this.setPosition( new cc.Point( pos.x + this.speed, pos.y ) );
			} else {
			    this.isChange = 2;
			}
    	}
    },

    pick: function( item ) {
    	this.item = new Item();
    	if ( item.name == 'Old Key' ) {
        	this.item.setStatus( item.type, item.name, 'res/Images/Items/OldKey.png' );
        } else if ( item.name == 'Crystal' ) {
        	this.item.setStatus( item.type, item.name, 'res/Images/Items/Crystal.png' );
        }
        this.itemList.push( item );
    },

    unCrouching: function() {
    	this.isCrouching = true;
    	this.isMove = false;
    	this.isCrouch = false;
    	for ( this.animeRound = 0; this.animeRound <= 30; this.animeRound++ ) {
	    	if ( this.animeRound <= 30 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/1L.png' ); }
    	}
    	this.isMove = true;
    	this.isCrouching = false;
    },

    crouching: function() {
    	this.isCrouching = true;
    	this.isMove = false;
    	for ( this.animeRound = 0; this.animeRound <= 30; this.animeRound++ ) {
	    	if ( this.animeRound <= 30 ) { this.initWithFile( 'res/Images/Player/Movement/Crouch/1L.png' ); }
    	}
    	this.isMove = true;
    	this.isCrouch = true;
    	this.isCrouching = true;
    },

    flip: function() {
    	if ( this.isRight ) {
	  		this.setFlippedX( true );
	   	} else if ( this.isLeft ) {
	   		this.setFlippedX( false );
	    } else if ( this.lastDirectionFacing == 'left' ) {
	    	this.setFlippedX( false );
	    } else if ( this.lastDirectionFacing == 'right' ) {
	    	this.setFlippedX( true );
	    }
    },

    passMove: function() {
        var pos = this.getPosition();    
        this.setPosition( new cc.Point( pos.x - this.speed, pos.y ) );
    },
});