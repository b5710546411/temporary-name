var Room = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/Room/room1-1b.bmp' );
        this.roomSize = this.getBoundingBoxToWorld();
        this.type = 'room';
        this.roomID = 1;
        this.changable = 1;
        /**
            0 = Unchangable
            1 = Changable to the left
            2 = Changabla to the right
            3 = Changable on both side
        */
    },

    setStatus: function( Effect, Dialog ) {
        this.effect = Effect;
        this.dialog = Dialog;
    },

    changeRoom: function( Player ) {
        var pPos = Player.getPosition();

        if ( this.roomID == 1 ) {
            this.initWithFile( 'res/Images/Passage/pass1-1.bmp' );
            this.roomInit( 3, 'pass', 2, Player);
        } else if ( this.roomID == 2 ) {
            if ( Player.isChange == 1 ) {
                this.initWithFile( 'res/Images/Room/room1-2.bmp' );
                this.roomInit( 2, 'room', 3, Player);
            } else {
                this.initWithFile( 'res/Images/Room/room1-1b.bmp' );
                this.roomInit( 1, 'room', 1, Player);
            }
        } else if ( this.roomID == 3 ) {
            this.initWithFile( 'res/Images/Passage/pass1-1.bmp' );
            this.roomInit( 3, 'pass', 2, Player);
        } else if ( this.roomID == 4 ) {
            if ( Player.isChange == 1 ) {
                this.initWithFile( 'res/Images/Room/room1-4a.bmp' );
                this.roomInit( 2, 'room', 6, Player);
            } else {
                this.initWithFile( 'res/Images/Passage/pass1-1.bmp' );
                this.roomInit( 3, 'pass', 2, Player);
                this.exitDoor( Player );
            }
        } else if ( this.roomID == 6 ) {
            this.initWithFile( 'res/Images/Passage/pass1-2.bmp' );
            this.roomInit( 3, 'pass', 4, Player);
        } else if ( this.roomID == 5 ) {
            this.initWithFile( 'res/Images/Passage/pass1-2.bmp' );
            this.roomInit( 3, 'pass', 4, Player);
            this.exitDoor( Player );
        } else if ( this.roomID == 7 ) {
            if ( Player.isChange == 1 ) {
                this.initWithFile( 'res/Images/Room/room2-1.bmp' );
                this.roomInit( 0, 'room', 2.1, Player);
            } else {
                this.initWithFile( 'res/Images/Room/room1-4b.bmp' );
                this.roomInit( 2, 'room', 6, Player);
                this.exitDoor( Player );
            }
        }
    },

    enterRoom: function( Player ) {
        var pPos = Player.getPosition();
        if ( this.roomID == 2 ) {
            this.initWithFile( 'res/Images/Passage/pass1-2.bmp' );
            Player.setPosition( new cc.Point( screenWidth, pPos.y ) );
            Player.isChange = 1;
            this.roomInit( 3, 'pass', 4, Player);
        } else if ( this.roomID == 4 ) {
            this.initWithFile( 'res/Images/Room/room1-3a.bmp' );
            Player.setPosition( new cc.Point( screenWidth, pPos.y ) );
            Player.isChange = 1;
            this.roomInit( 2, 'room', 5, Player);
        } else if ( this.roomID == 6 ) {
            this.initWithFile( 'res/Images/Passage/pass1-3.bmp' );
            Player.setPosition( new cc.Point( screenWidth, pPos.y ) );
            Player.isChange = 1;
            this.roomInit( 3, 'pass', 7, Player);
        }
    },

    exitDoor: function( Player ) {
        var pPos = Player.getPosition();
        var pos = this.getPosition();

        Player.setPosition( new cc.Point( screenWidth/2, pPos.y ) );
        this.setPosition( new cc.Point( screenWidth/2, pos.y ) );
        var range = this.door.getPosition().x - pos.x;
        this.setPosition( new cc.Point( screenWidth/2 - range, pos.y ) );
        console.log( range, this.getPosition().x);
    },

    roomSetup: function( Player ) {

        if ( Player.isChange == 1 ) {
            Player.setPosition( new cc.Point( screenWidth, Player.getPosition().y ) );
            //this.setPosition( new cc.point( screenWidth/2 - ( roomSize.width - screenWidth )/2 - pos.x, pos.y + screenHeight/2 ) );
        } else if ( Player.isChange == 2 ) {
            Player.setPosition( new cc.Point( 0, Player.getPosition().y ) );
            //this.setPosition( new cc.point( screenWidth/2 + ( roomSize.width - screenWidth )/2 - pos.x, pos.y + screenHeight/2 ) );
        }

        this.removeChild( this.door );
        this.removeChild( this.item );
        this.removeChild( this.enemy );
        this.removeChild( this.trap );
        this.removeChild( this.leviathan );
        this.effect.initWithFile( 'res/Images/null.png' )
//        this.door = new Effect();
  //      this.item = new Effect();
    //    this.enemy = new Effect();
      //  this.trap = new Effect();
        if ( this.roomID == 1 ) {/*
            this.enemy = new Enemy();
            this.enemy.setStatus( 'RedCrawler', Player );
            this.enemy.setPosition( 200, 150 );
            this.addChild( this.enemy );
            this.enemy.scheduleUpdate();*/
        } else if ( this.roomID == 2 ) {
            this.addDoor( 410 );
            this.door.isLock = true;
            for ( var i = 0; i < Player.usedItemList.length; i++ ) {
                if ( Player.usedItemList[i].name == 'Old Key' ) {
                    this.door.isLock = false;
                }
            }

            this.addEnemy( 'RedCrawler', Player, 200, 150 );
        } else if ( this.roomID == 3 ) {
            this.addItem( 'key', 'Old Key', 'res/Images/Items/Items(yellow).png', 200, screenHeight/2 - 150 );
            this.addTrap( 'Trap1', 'passive', 130, Player, 150, screenHeight/2 );
        } else if ( this.roomID == 4 ) {
            this.addDoor( 530 );
            this.addEnemy( 'FlyingSucker', Player, 200, 450 );
        } else if ( this.roomID == 5 ) {
            this.addItem( 'special', 'Crystal', 'res/Images/Items/Items(blue).png', 170, screenHeight/2 + 5 );
            this.addTrap( 'Trap2', 'passive', 100, Player, screenWidth/2 - 50, screenHeight/2 );
        } else if ( this.roomID == 6 ) {
            this.addDoor( screenWidth/2 );
            this.door.isLock = true;
            for ( var i = 0; i < Player.usedItemList.length; i++ ) {
                if ( Player.usedItemList[i].name == 'Crystal' ) {
                    this.door.isLock = false;
                    this.initWithFile( 'res/Images/Room/room1-4b.bmp' );
                }
            }

            this.addTrap( 'Trap3', 'active', 130, Player, 150, screenHeight/2 );
        } else if ( this.roomID == 7 ) {
            this.dialog.face.setVisible( false );
            this.dialog.face.icon.setVisible( false );
            this.setPosition( new cc.Point( 0, screenHeight/2 ) );
            this.leviathan = new Leviathan();
            this.leviathan.setStatus( Player, this );
            this.addChild( this.leviathan );
            this.leviathan.scheduleUpdate();
        } else if ( this.roomID == 2.1 ) {
            this.effect.initWithFile( 'res/Images/Filter(Black).bmp' );
            this.dialog.speak( 'Stage1 End' );
        } else {

        }
    },

    roomInit: function( changable, type, roomID, Player ) {
        this.changable = changable;
        this.type = type;
        if ( this.type == 'pass' ) {
            if ( Player.isChange == 1 ) {
                this.setPosition( new cc.Point( 200, screenHeight/2 ) );
            } else if ( Player.isChange == 2 ) {
                this.setPosition( new cc.Point( 600, screenHeight/2 ) );
            }
        } else {
            this.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) );
        }
        this.roomID = roomID;
        this.roomSetup( Player );
        this.roomSize = this.getBoundingBoxToWorld();
        Player.isChange = 0;
    },

    addEnemy: function( name, Player, posX, posY ) {
        this.enemy = new Enemy();
        this.enemy.setStatus( name, Player, this, this.effect, this.dialog );
        this.enemy.setPosition( posX, posY );
        this.addChild( this.enemy );
        this.enemy.scheduleUpdate();
    },

    addTrap: function( name, type, range, Player, posX, posY ) {
        this.trap = new Trap();
        this.trap.setStatus( name, type, range, Player, this, this.effect, this.dialog );
        this.trap.setPosition( new cc.Point( posX, posY ) );
        this.addChild( this.trap );
        this.trap.scheduleUpdate();
    },

    addDoor: function( posX ) {
        this.door = new Door();
        this.door.setRoom( this );
        this.door.setPosition( new cc.Point( posX, screenHeight/2 ) );
        this.addChild( this.door );;
    },

    addItem: function( type, name, path, posX, posY ) {
        this.item = new Item();
        this.item.setStatus( type, name, path );
        this.item.setPosition( new cc.Point( posX, posY ) );
        this.addChild( this.item );
    },

    passMove: function( speed ) {
        var pos = this.getPosition();
        this.setPosition( new cc.Point( pos.x - speed, pos.y ) );
    },
});