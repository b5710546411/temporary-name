var Trap = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.name = '';
        this.range = 0;
        this.type = 'active';
        this.isTriggered = false;
        this.animeRound = 0;
    },

    setStatus: function( name, type, range, Player, room, Effect, Dialog ) {
        this.name = name;
        this.player = Player;
        this.type = type;
        if ( this.type == 'passive' ) {
            this.isTriggered = true;
        } else {
            this.isTriggered = false;
        }
        this.range = range;
        this.player = Player;
        this.room = room;
        this.effect = Effect;
        this.dialog = Dialog;
    },

    update: function() {
        if ( this.type == 'passive' ) {
            if ( this.closeTo() ) {
                if ( this.name == 'Trap1' ) {
                    if ( !this.player.isCrouch ) {
                        this.player.isDeath = true;
                        this.playDeathAnimetion();
                    }
                } else if ( this.name == 'Trap2' ) {
                    if ( !this.player.isRun ) {
                        this.player.isDeath = true;
                        this.playDeathAnimetion();
                    }
                }
            }
        } else if ( this.isTriggered ) {
            if ( this.name == 'Trap3' ) {    
                this.playDeathAnimetion();
            }
        }
    },

    closeTo: function() {
		var myPos = this.getPosition();
		var oPos = this.player.getPosition();
	  	return ( Math.abs( myPos.x - oPos.x ) <= this.range );
    },
 
    playDeathAnimetion: function() {
        var pPos = this.player.getPosition();
        var pos = this.getPosition();
        this.animeRound++;
        this.dialog.face.setVisible( false );
        this.dialog.face.icon.setVisible( false );
        if ( this.name == 'Trap1' ) {
            if ( this.animeRound <= 10 ) { this.player.initWithFile( 'res/Images/Player/Trap1/1.png' ); this.effect.setPosition( new cc.Point( pPos.x, pPos.y ) ); }
            else if ( this.animeRound <= 20 ) { this.player.initWithFile( 'res/Images/Player/Trap1/2.png' ); }
            else if ( this.animeRound <= 30 ) { this.player.initWithFile( 'res/Images/Player/Trap1/3.png' ); }
            else if ( this.animeRound <= 130 ) { this.player.initWithFile( 'res/Images/Player/Trap1/4.png' ); }
            else if ( this.animeRound <= 230 ) { this.player.initWithFile( 'res/Images/Player/Trap1/5.png' ); }
            else if ( this.animeRound <= 250 ) { this.player.initWithFile( 'res/Images/Player/Trap1/6.png' ); }
            else if ( this.animeRound <= 330 ) { this.player.initWithFile( 'res/Images/Player/Trap1/7.png' ); }
            else if ( this.animeRound <= 360 ) { this.effect.initWithFile( 'res/Images/Player/Trap1/Blood1.png' ); }
            else if ( this.animeRound <= 390 ) { this.effect.initWithFile( 'res/Images/Player/Trap1/Blood2.png' ); }
            else if ( this.animeRound <= 420 ) { this.effect.initWithFile( 'res/Images/Player/Trap1/Blood3.png' ); }
            else { this.animeRound = 330; }
        } else if ( this.name == 'Trap2' ) {
            if ( this.animeRound <= 1 ) {
                this.room.initWithFile( 'res/Images/Room/room1-3b.bmp' );
                this.player.setPosition( screenWidth/2 - 50, pPos.y - 600 )
                this.player.initWithFile( 'res/Images/Player/Trap2/1.png' );
            } else if ( this.animeRound <= 120 ) {
            } else if ( this.animeRound <= 300 ) {
                this.room.setPosition( new cc.Point( this.room.getPosition().x, this.room.getPosition().y + 3 ) );
                this.player.setPosition( new cc.Point( pPos.x, pPos.y + 3 ) );
            }
        } else if ( this.name == 'Trap3' ) {
            if ( this.animeRound <= 1 ) {
                this.effect.setPosition( new cc.Point( screenWidth/2 , 760 ) );
                this.effect.initWithFile( 'res/Images/Room/room1-4c.png' );
                this.room.changable = 0;
                this.dialog.face.setVisible( true );
                this.dialog.face.icon.setVisible( true );
                this.dialog.speak('Trap3');
            } else if ( this.animeRound == 200 ) {
                this.dialog.face.setVisible( true );
                this.dialog.face.icon.setVisible( true );
                this.dialog.talk();
            } else if ( this.animeRound <= 3500 ) {
                this.effect.setPosition( new cc.Point( this.effect.getPosition().x, this.effect.getPosition().y - screenHeight/6000 ) );
            } else if ( this.animeRound <= 3700 ) {
                this.player.isDeath = true;
                this.player.initWithFile( 'res/Images/Player/Movement/Fear/Sit.png' )
                if ( this.animeRound % 2 == 0 ) {
                    this.player.setPosition( new cc.Point( pPos.x + 2, pPos.y ) )
                } else if ( this.animeRound % 2 == 1 ) {
                    this.player.setPosition( new cc.Point( pPos.x - 2, pPos.y ) )
                } if ( this.animeRound == 3700 ) { this.dialog.face.setVisible( true ); this.dialog.face.icon.setVisible( true ); this.dialog.talk(); }
            } else if ( this.animeRound <= 3711 ) {
                this.effect.setPosition( new cc.Point( this.effect.getPosition().x, this.effect.getPosition().y - 10 ) );
            } else if ( this.animeRound == 3712 ) { this.dialog.talk();
            } else if ( this.animeRound <= 3900 ) {
            } else if ( this.animeRound <= 4100 ) {
                this.effect.initWithFile( 'res/Images/Filter(Red).bmp' );
                this.player.initWithFile( 'res/Images/null.png' )
            } else if ( this.animeRound == 4101 ) {
                this.player.initWithFile( 'res/Images/Player/Trap3/1.png' )
                this.effect.initWithFile( 'res/Images/null.png' );
            }

            if ( this.animeRound >= 2600 && this.animeRound <= 3500 ) {
                this.player.isCrouch = true;
            }
        }
    }
});