//First Pleses be noted that in this code 0, 1, 2, 3 usually means directions.

var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 0, 0, 0, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );

        this.room = new Room();
        this.room.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) )
        this.addChild( this.room );

        this.player = new Player();
        this.player.setPosition( new cc.Point( 500, 250 ) );
        this.addChild( this.player );
        this.player.scheduleUpdate();

        this.itemSelecter = new Item();
        this.itemSelecter.setPosition( new cc.Point( 40, 40 ) );
        this.addChild( this.itemSelecter );
        this.i = -1;
        this.itemImageList = [];

        this.conversation = cc.LabelTTF.create( '', 'Arial', 32 );
        this.conversation.setPosition( new cc.Point( 302, 500 ) );
        this.conversation.setDimensions( 540, 160 );
        this.conversation.setHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT );
        this.conversation.setVerticalAlignment( cc.VERTICAL_TEXT_ALIGNMENT_TOP );
        this.addChild( this.conversation, 1 );

        this.effect = new Effect();
        this.effect.initWithFile( 'res/Images/Filter(Black).bmp' );
        this.addChild( this.effect );

        this.reserveTextField = new cc.LabelTTF.create( '', 'Arial', 60 );
        this.reserveTextField.setPosition( new cc.Point( 400, 500 ) );
        this.reserveTextField2 = new cc.LabelTTF.create( '', 'Arial', 32 );
        this.reserveTextField2.setPosition( new cc.Point( 400, 300 ) );
        this.addChild( this.reserveTextField );
        this.addChild( this.reserveTextField2 );

        this.faceImage = new Effect();
        this.faceImage.setPosition( new cc.Point( screenWidth - 110, screenHeight - 100 ) );
        this.addChild( this.faceImage );

        this.face = new Face();
        this.face.setPosition( new cc.Point( screenWidth - 110, screenHeight - 100 ) );
        this.face.setStatus( this.player, this.faceImage );
        this.addChild( this.face );
        this.face.scheduleUpdate();

        this.dialog = new Dialog();
        this.dialog.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) );
        this.addChild( this.dialog );
        this.dialog.setStatus( this.conversation, this.room, this.player, this.face, this.effect, this.reserveTextField, this.reserveTextField2 );
        this.dialog.setVisible( false );
        this.room.setStatus( this.effect, this.dialog );

        this.addKeyboardHandlers();
        this.scheduleUpdate();

        this.dialog.speak('StartDialog');

        return true;
    },

    update: function() {
        var pos = this.player.getPosition();
        var roomSize = this.room.getBoundingBoxToWorld();
        
        if ( this.room.type == 'pass' ) {
            if ( ( pos.x < screenWidth/2 && pos.x > screenWidth/2 - 6 ) ) { 
                if ( roomSize.x <= 0 ) {
                    if ( this.player.isLeft ) {
                        this.passMove();
                    }
                }
            }
            if ( ( pos.x < screenWidth/2 + 6 && pos.x > screenWidth/2 ) ) { 
                if ( roomSize.x >=  screenWidth - roomSize.width ) {
                    if ( this.player.isRight ) {
                        this.passMove();
                    }
                }
            }
        }

        if ( this.player.isChange == 1 ) {
            if ( this.room.changable == 1 || this.room.changable == 3 ) {
                this.room.changeRoom( this.player );
            }
        } else if ( this.player.isChange == 2 ) {
            if ( this.room.changable == 2 || this.room.changable == 3 ) {
                this.room.changeRoom( this.player );
            }
        }
    },

    passMove: function() {
        if ( this.player.isMove ) {
            this.player.passMove();
            this.room.passMove( this.player.speed );
        }
    },

    onKeyDown: function( keyCode, event ) {
        if ( keyCode == 37 ) {
            if ( this.player.isUsingItem == false ) {
                this.player.isLeft = true;
                this.player.isRight = false;
            } else {
                if ( this.i != -1 ) {
                    this.i -= 1;
                    this.itemSelecter.setPosition( new cc.Point( this.itemSelecter.getPosition().x - 80, 40 ) );
                }
            }
        } else if ( keyCode == 39 ) {
            if ( this.player.isUsingItem == false ) {
                this.player.isLeft = false;
                this.player.isRight = true;
            } else {
                if ( this.i != 7 ) {
                    this.i += 1;
                    this.itemSelecter.setPosition( new cc.Point( this.itemSelecter.getPosition().x + 80, 40 ) );
                }
            }
        } else if ( keyCode == 88 ) {
            if ( this.player.isCrouch == false ) {
                this.player.isRun = true;
            }
        } else if ( keyCode == 40 ) {
            if ( this.player.isRun == false ) {
                if ( this.player.isDeath == false ) {
                    this.player.crouching();
                }
            }
        } else if ( keyCode == 38 ) {
            if ( this.room.door.closeTo( this.player ) ) {
                if ( this.room.door.isLock == false ) {
                    this.room.enterRoom( this.player );
                } else {
                    cc.director.pause();
                    this.conversation.setString( 'The door is locked!!!' );
                }
            }
        } else if ( keyCode == 65 ) {
            if ( this.face.icon.isVisible() ) {
                this.face.icon.setVisible( false );
                this.face.setVisible( false );
            } else {
                this.face.icon.setVisible( true );
                this.face.setVisible( true );
            }
        } else if ( keyCode == 90 ) {
            if ( this.dialog.isTalk ) {
                this.dialog.talk();
            } else if ( this.conversation.getString() != '' ) {
                cc.director.resume();
                this.conversation.setString( '' );
            } else if ( this.room.trap.closeTo() ) {
                this.room.trap.isTriggered = true;
            } else if ( this.room.item.closeTo( this.player ) ) {    
                this.player.pick( this.room.item );
                this.room.removeChild( this.room.item );
                this.room.item.setPosition( new cc.Point( this.room.item.getPosition().x, this.room.item.getPosition().y + 2000 ) );
                this.addItem( this.player.itemList[this.player.itemList.length - 1] );
                cc.director.pause();
                this.conversation.setString( 'You pick up an ' + this.room.item.name + '.' );
            }
        } else if ( keyCode == 32 ) {
            this.player.isUsingItem = true;
        }
    },

    addItem: function( item ) {
        this.item = new Item();
        if ( item.name == 'Old Key' ) {
            this.item.setStatus( item.type, item.name, 'res/Images/Items/OldKey.png' );
        } else if ( item.name == 'Crystal' ) {
            this.item.setStatus( item.type, item.name, 'res/Images/Items/Crystal.png' );
        }
        this.item.setPosition( new cc.Point( 40 + ( this.player.itemList.length * 80 ), 40 ) );
        this.addChild( this.item );
        this.itemImageList.push( this.item );
    },

    removeItem: function( index ) { 
        if (index > -1) {
            console.log(this.itemImageList[index].name);
            this.removeChild( this.itemImageList[index] );
            this.itemImageList.splice(index, 1);
        }
        console.log(this.player.itemList, this.itemImageList, index)
        for (var i = index; i < this.itemImageList.length; i++) {
            this.itemImageList[i].setPosition( new cc.Point( this.itemImageList[i].getPosition().x - 80, 40 ) );
        };
    },

    onKeyUp: function( keyCode, event ) {
        if ( keyCode == 37 ) {
            this.player.isLeft = false;
            if ( this.player.isUsingItem == false ) {
                this.player.lastDirectionFacing = 'left';
            }
        } else if ( keyCode == 39) {
            this.player.isRight = false;
            if ( this.player.isUsingItem == false ) {
                this.player.lastDirectionFacing = 'right';
            }
        } else if ( keyCode == 88 ) {
            this.player.isRun = false;
        } else if ( keyCode == 40) {
            if ( this.player.isDeath == false ) {
                this.player.unCrouching();
            }
        } else if ( keyCode == 82 ) {
            if ( this.player.isDeath && !this.dialog.isVisible() ) {
                this.restart();
            }
        } else if ( keyCode == 32 ) {
            console.log(this.player.itemList, this.itemImageList)
            if ( this.i >= 0 && this.i <= this.player.itemList.length - 1 ) {
                if ( this.player.itemList[this.i].use( this.player, this.room ) ) {
                    this.removeItem( this.i );
                }
            }
            console.log(this.player.itemList, this.itemImageList)
            this.i = -1;
            this.itemSelecter.setPosition( new cc.Point( 40, 40 ) );
            this.player.isUsingItem = false;
        }
        console.log( 'Up: ' + keyCode.toString() );
    },

    restart: function() {
        this.room.initWithFile( 'res/Images/Room/room1-1b.bmp' );
        this.room.roomInit( 1, 'room', 1, this.player );
        this.room.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) );
        this.player.isDeath = false;
        this.player.isCrouch = false;
        this.player.HP = 100.00;
        this.player.setPosition( new cc.Point( 500, 250 ) );
        this.player.deathCount++;
        this.effect.initWithFile( 'res/Images/null.png' );
        this.face.setVisible( true );
        this.face.icon.setVisible( true );
    },

    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased : function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    }
});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        console.log( 'GameLayer created' );
        layer.init();
        this.addChild( layer );
    }
});