var Enemy = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Images/Enemy/N2/1R.png' );
        this.speed = 1.25;
        this.isKill = false
        this.animeRound = 0;
    },

    setStatus: function( type, Player, Room, Effect, Dialog ) {
        this.player = Player;
        this.room = Room;
        this.type = type;
        if ( this.type == 'RedCrawler' ) {
            this.speed = 1.25;
        } else if ( this.type == 'FlyingSucker' ) {
            this.speed = 3;
        }
        this.effect = Effect;
        this.dialog = Dialog;
    },

    update: function() {
        console.log(Math.floor(this.player.timeUsed/60))
        this.animeRound++;
        if ( this.closeTo( this.player ) ) {
            if ( this.player.HP > 0 ) {
                this.attack();
            }
        } else if ( !this.isKill ) {
            this.move( this.player );
        }

        if ( this.isKill ) {
            this.playDeathAnimetion();
        }
    },

    move: function( Player ) {
        var pos = this.getPosition();
        var pPos = this.player.getPosition();

        if ( this.type == 'RedCrawler' ) {
            if ( this.animeRound <= 40 ) { this.initWithFile( 'res/Images/Enemy/N2/1L.png' ); }
            else if ( this.animeRound <= 80 ) { this.initWithFile( 'res/Images/Enemy/N2/2L.png' ); }
            else if ( this.animeRound <= 120 ) { this.initWithFile( 'res/Images/Enemy/N2/3L.png' ); }
            else { this.animeRound = 0; }
            if ( pPos.x - pos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) > 0 ) {
                this.setPosition( new cc.Point( pos.x + this.speed, pos.y ) );
                this.setFlippedX( true );
            } else if ( pPos.x - pos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) < 0 ) {
                this.setPosition( new cc.Point( pos.x - this.speed, pos.y ) );
                this.setFlippedX( false );
            }
        }if ( this.type == 'FlyingSucker' ) {
            if ( this.detect() ) {
                this.animeRound += 0.5;
                this.speed = 5;
                if ( pPos.x - pos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) > 0 ) {
                    this.setPosition( new cc.Point( pos.x + this.speed, pos.y ) );
                    this.setFlippedX( true );
                } else if ( pPos.x - pos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) < 0 ) {
                    this.setPosition( new cc.Point( pos.x - this.speed, pos.y ) );
                    this.setFlippedX( false );
                }
            } else {
                this.speed = 3;
                if ( this.animeRound <= 45 ) {
                    this.setPosition( new cc.Point( pos.x + this.speed, pos.y ) );
                    this.setFlippedX( true );
                } else if ( this.animeRound <= 90 ) {
                    this.setPosition( new cc.Point( pos.x - this.speed, pos.y ) );
                    this.setFlippedX( false );
                }
            }
            if ( this.animeRound <= 30 ) { this.initWithFile( 'res/Images/Enemy/N1/1L.png' ); }
            else if ( this.animeRound <= 60 ) { this.initWithFile( 'res/Images/Enemy/N1/2L.png' ); }
            else if ( this.animeRound <= 90 ) { this.initWithFile( 'res/Images/Enemy/N1/3L.png' ); }
            else { this.animeRound = 0; }
        }
    },

    attack: function() {
        if ( this.type == 'RedCrawler' ) {
            if ( Math.abs( this.player.speed ) != 5 ) {
                this.player.HP -= 1.00;
                if ( this.player.HP <= 0 ) {
                    this.isKill = true;
                    this.player.isDeath = true;
                    this.animeRound = 0;
                }
            }
        } else if ( this.type == 'FlyingSucker' ) {
            if ( this.detect() ) {
                this.player.HP -= 1.00;
                console.log(this.player.HP)
                if ( this.player.HP <= 0 ) {
                    this.isKill = true;
                    this.player.isDeath = true;
                    this.animeRound = 0;
                }
            } else {
                this.move( this.player );
            }
        }
    },

    playDeathAnimetion: function() {
        var pPos = this.player.getPosition();
        if ( this.type == 'RedCrawler' ) {
            if ( this.animeRound == 0 ) { 
                this.player.initWithFile( 'res/Images/Player/RedCrawler/Grab4.png' );
                this.initWithFile( 'res/Images/null.png' )
                this.dialog.speak( 'RedCrawler' );
                this.effect.setPosition( new cc.Point( screenWidth/2 - this.getPosition().x, screenHeight/2 ) );
            } else if ( this.animeRound == 120 ) { 
                this.player.initWithFile( 'res/Images/Player/RedCrawler/1.png' );
                this.effect.initWithFile( 'res/Images/null.png' );
            }

            if ( this.animeRound % 120 <= 39 ) { this.initWithFile( 'res/Images/Enemy/N2/1L.png' ); }
            else if ( this.animeRound % 120 <= 79 ) { this.initWithFile( 'res/Images/Enemy/N2/2L.png' ); }
            else if ( this.animeRound % 120 <= 119 ) { this.initWithFile( 'res/Images/Enemy/N2/3L.png' ); }
        } else if ( this.type == 'FlyingSucker' ) {
            if ( this.animeRound <= 10 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/1.png' ); }
            else if ( this.animeRound <= 20 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/2.png' ); }
            else if ( this.animeRound <= 50 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/3.png' ); }
            else if ( this.animeRound <= 60 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/4.png' ); }
            else if ( this.animeRound <= 70 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/5.png' ); }
            else if ( this.animeRound <= 80 ) { this.player.initWithFile( 'res/Images/Player/FlyingSucker/6.png' ); }
        }
    },

    detect: function() {
        var myPos = this.getPosition();
        var oPos = this.player.getPosition();
        if ( this.player.isCrouch ) {
            return false;
        }
        return ( Math.abs( myPos.x - ( oPos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) ) ) <= 200 );
    },

    closeTo: function( Player ) {
		var myPos = this.getPosition();
		var oPos = Player.getPosition();
	  	return ( Math.abs( myPos.x - ( oPos.x - ( this.room.getPosition().x - ( 1200/*this.room.roomSize.width*/ - screenWidth )*1.5 ) ) ) <= 40 );
    },
 
//    randomPosition: function() {
//       	this.setPosition( new cc.Point( Math.floor(Math.random() * 750), screenHeight + 150 ) );
//    } 
});